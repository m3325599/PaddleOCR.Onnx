﻿using PaddleOCR.Onnx;

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnnxConsoleDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] extensions = new string[] { ".jpg", ".bmp", ".jpeg", ".png", ".tif", ".tiff" };
            string imageroot = Environment.CurrentDirectory + "\\image";
            if (!Directory.Exists(imageroot)) Directory.CreateDirectory(imageroot);
            DirectoryInfo directoryInfo = new DirectoryInfo(imageroot);
            var files = directoryInfo.GetFiles("*.*");

            OCRParameter oCRParameter = new OCRParameter();
            oCRParameter.MaxSideLen = 1200;
            oCRParameter.numThread = 6;
            oCRParameter.Enable_mkldnn = true;
            oCRParameter.use_gpu = false;
            oCRParameter.use_angle_cls = false;
            oCRParameter.visualize = false;
            OCRModelConfig config = null;
            //轻量版中英文模型V3
            //OCRModelConfig config = new OCRModelConfig();
            //string root = Environment.CurrentDirectory;
            //string modelPathroot = root + @"\inference_v3_onnx";
            //config.det_infer = modelPathroot + @"\ch_PP-OCRv3_det_infer.onnx";
            //config.rec_infer = modelPathroot + @"\ch_PP-OCRv3_rec_infer.onnx";
            //config.cls_infer = modelPathroot + @"\ch_ppocr_mobile_v2.0_cls_infer.onnx";
         
            //config.keys = modelPathroot + @"\ppocr_keys.txt";


            PaddleOCREngine paddleOCREngine = new PaddleOCREngine(config, oCRParameter);
            int count = 1;
            //DateTime dt1 = DateTime.Now;
            //DateTime dt2 = DateTime.Now;
            OCRResult result = null;

            for (int i = 0; i < 1; i++)
            {
                foreach (var file in files)
                {
                    Console.WriteLine($"第{count}个文件");
                    {
                        if (!extensions.Contains(file.Extension.ToLower())) continue;

                        //Task.Run(() =>
                        //{

                            DateTime dt1 = DateTime.Now;
                            result = paddleOCREngine.DetectText(file.FullName);

                            // result = paddleOCREngine.DetectText(File.ReadAllBytes(   file.FullName));
                            DateTime dt2 = DateTime.Now;

                            Console.WriteLine(result.Text + "\n");

                            Console.WriteLine((dt2 - dt1).TotalMilliseconds + "ms\n");

                            GC.Collect();
                            count++;
                       // });
                    }
                }
            }
            Console.ReadLine();
        }

    }
}
